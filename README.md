# DevTools

#### 介绍
开发一些Qt工具，方便使用。

#### 1. FindDLL工具

FindDLL可以根据依赖库的PATH路径，自动分析 exe 或 dll 文件的依赖库，可以递推查找所有依赖dll，并支持复制到指定文件夹，解决程序逐个逐层寻找依赖dll的麻烦。

FindDLL并专门针对 Qt 程序，设置Qt开发环境中的windeployqt.exe路径、 plugins 路径和 qml 路径，复制所有的插件到指定目录，以满足 Qt 程序依赖项。

适应Qt官方开发环境的依赖关系复制和MSYS2开发环境的依赖关系复制，只需要手动添加开发环境中Qt动态库所在的bin路径。
